/**
 * Created by jayse on 6/1/16.
 * Francis wrote the following methods in this class: SYNC, DELETEFTE
 * Jayse wrote the following methods in this class: Constructor, FALLOC, FREE, FEMPTY, ISOPEN
 */
import java.util.Vector;

public class FileTable
{
    private Vector table;         // the actual entity of this file table
    private Directory dir;        // the root directory

    public FileTable( Directory directory )
    {
        table = new Vector();     // instantiate a file (structure) table
        dir = directory;           // receive a reference to the Directory
    }                              // from the file system

    // major public methods
    //changing it only two flags: 0 - unused, 1 - read, 2 write
    public synchronized FileTableEntry falloc( String filename, String mode )
    {
        short iNumber = -1;
        Inode inode = null;

        while (true)
        {
            iNumber = (filename.equals("/")) ? 0 : dir.namei(filename);

            if (iNumber >= 0)
            {
                inode = new Inode(iNumber, dir.getDirBlocks());

                if (mode.equals("r") && inode.flag == 1)
                    break;
                else if (inode.flag == 1)
                {
                    inode.flag = 2;
                    break;
                }
                else if (inode.flag == 2)
                {
                    try
                    {
                        wait();
                    }
                    catch (InterruptedException e)
                    {
                    }
                }
                else if (inode.flag == 3)
                {
                    iNumber = -1;
                    return null;
                }

            }

            if (mode.equals("r"))
                return null;

            iNumber = dir.ialloc(filename);
            inode = new Inode(iNumber, dir.getDirBlocks());
            inode.flag = 2;
        }

        inode.count++;
        inode.toDisk(iNumber, dir.getDirBlocks());
        FileTableEntry e = new FileTableEntry(inode, iNumber, mode);
        table.addElement(e);
        return e;
    }

    public synchronized boolean ffree( FileTableEntry e )
    {
        if (table.removeElement(e))  //if it was in the table
        {
            e.inode.count--;
            switch (e.inode.flag)
            {
                case 1://read
                {
                    if (e.inode.count == 0)
                        e.inode.flag = 0;//set to unused
                    else
                        e.inode.flag = 3;   //to be deleted
                    break;
                }
                case 2://write
                {
                    if (e.inode.count == 0)
                        e.inode.flag = 0;
                    else
                        e.inode.flag = 3;   //to be deleted
                    break;
                }
                case 3://delete
                {

                }
                default:
                    break;
            }
            e.inode.toDisk(e.iNumber, dir.getDirBlocks());
            e = null;
            this.notify();
            return true;
        }
        return false;
    }

    //Impact : returns true is the shared file table is empty, otherwise false
    public synchronized boolean fempty( ) {
        return table.isEmpty( );  // return if table is empty
    }

    //Impact : returns true if the file is listed in the shared file table, otherwise false
    public boolean isOpen (FileTableEntry fd) {
        return table.contains(fd);
    }

    //Impact : commits all modified files to disk
    public synchronized void sync ()
    {
        //get size of current table
        int tableSize = table.size();

        //iterate through table to check for a commit condition
        for (int i = 0; i <tableSize; i++)
        {
            FileTableEntry fde = (FileTableEntry)table.elementAt(i);
            if ((fde.mode == "w") || (fde.mode == "w+") || (fde.mode == "a"))
            {
                fde.inode.toDisk(fde.iNumber, dir.getDirBlocks());
            }
        }
    }

    //Impact : find all file table entries that correspond to a name and
    // removes them from the table
    public synchronized void deleteFTE (String fileName)
    {
        int i = 0;
        FileTableEntry fde = (FileTableEntry)table.elementAt(i);


    }
}
