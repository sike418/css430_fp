/**
 * Created by francis on 6/1/16.
 * Francis wrote all methods within this class
 */
//SuperBlock class represents the superblock data at sector zero (0) of a disk
public class SuperBlock
{
    private final int defaultInodesBlocks = 64;     //default number of inodes
    private int totalBlocks = 1000;                 //disk block count
    private  final int maxINodes = 841;             //based on the consumption of 9.5 blocks per 8 inodes
    private int totalInodes;                        //inode count
    private int freeList;                           //queue of avail disk blocks

    //Constructor
    public SuperBlock(int diskSize)
    {
        //create block array in mem and load superblock data
        byte[] superBlock = new byte[Disk.blockSize];
        SysLib.rawread(0, superBlock);

        totalBlocks = SysLib.bytes2int(superBlock, 0);   //read superblock info from disk
        totalInodes = SysLib.bytes2int(superBlock, 4);   //read inode info from disk
        freeList = SysLib.bytes2int(superBlock, 8);      //read freelist info from disk

        //determine if the raw disk contents are valid and, format disk if not
        if (!((totalBlocks == diskSize) && (totalInodes > 0) && (freeList >= 2))) {
            //new or corrupted disk - format required
            totalBlocks = diskSize;
            format(defaultInodesBlocks);
        }
    }

    //returns the max number of inodes
    public int maxInode () { return totalInodes; }

    //overloaded FORMAT method - no parameters
    //Impact : summon overloaded FORMAT method with one parameter
    public synchronized void format()
    {
        format(defaultInodesBlocks);
    }

    //overloaded FORMAT method - one parameter
    //Parameter #1: number of INodes expected on the disk
    //Impact : clear disk of data, reset superblock parameters based on new inode count, and update freelist
    public synchronized boolean format(int newInodeCount)
    {
        //ensure file count is positive and doesn't exceed system max
        if ((newInodeCount <= maxINodes) && (newInodeCount > 0))
        {
            //set the total number of inodes
            totalInodes = newInodeCount;

            //clear all block data
            //create a temporary byte array of block size
            byte[] tempBlock = new byte[Disk.blockSize];
            //iteratively clear out block data
            for (int i = 0; i < Disk.blockSize; i++) { tempBlock[i] = 0; }

            //define starting block for freelist
            //shift freelist pointer ot make room for the directory blocks
            freeList = (int)(totalInodes/(Disk.blockSize/64));
            if ((totalInodes%(Disk.blockSize/64)) != 0) freeList++;
            //shift freelist pointer to make room for the inode blocks
            freeList = freeList + (int)(totalInodes/(Disk.blockSize/32)) + 1;
            if ((totalInodes%(Disk.blockSize/32)) != 0) freeList++;

            //iteratively write updated block data to freelist disk blocks
            for (int nextBlock = freeList; nextBlock < totalBlocks; nextBlock++)
            {
                //update current block's pointer to next block for freelist integrity
                //update next block pointer in memory
                SysLib.int2bytes(nextBlock, tempBlock, 0);
                //commit pointer info to disk
                SysLib.rawwrite(nextBlock, tempBlock);
            }

            sync();
            return true;
        }
        else
        {
            SysLib.cerr("File count for format is out of bounds - no changes made.  Value range is 0 to 841.");
            return false;
        }
    }

    //Impact : writes/commits superblock data from memory to disk
    public synchronized void sync ()
    {
        //create temp array
        byte[] superBlock = new byte [Disk.blockSize];

        //load superblock data, converted into bytes, into temp array
        SysLib.int2bytes(totalBlocks, superBlock, 0);
        SysLib.int2bytes(totalInodes, superBlock, 4);
        SysLib.int2bytes(freeList, superBlock, 8);

        //write temp array data to disk
        SysLib.rawwrite(0, superBlock);
    }

    //Impact : provides the blockID of a block dequeued from the freelist.  returns a '-1' on failure
    public synchronized int getFreeBlock ()
    {
        int blockID = freeList;    //initialize return value

        //check freelist for avail blocks, and if no blocks avail return a '-1'
        if (blockID != -1)
        {
            //otherwise, identify the avail block, remove from freelist
            byte[] tempBlock = new byte[Disk.blockSize];  //create temp byte array of block size
            SysLib.rawread(freeList, tempBlock);          //populate array with current free block pointer info
            freeList = SysLib.bytes2int(tempBlock, 0);    //convert bytes into next free blockID
            SysLib.int2bytes(-1, tempBlock, 0);           //reset the current block's next pointer
            SysLib.rawwrite(blockID, tempBlock);          //sync the block to disk
        }
        return blockID;
    }

    //Impact : receives a block released from an inode
    public synchronized boolean returnBlock (int blockNumber)
    {
        //initialize return value
        boolean returned = false;

        //check if blockNumber is within correct range of the disk and file system - return false if out of bounds
        int lowEnd = (int)(totalInodes/(Disk.blockSize/64));
        if ((totalInodes%(Disk.blockSize/64)) != 0) lowEnd++;

        //if blockNumber is in range
        if ((lowEnd <= blockNumber) && (blockNumber >= totalBlocks))
        {
            //add the block to the freelist queue and update the block contents
            byte[] tempBlock = new byte[Disk.blockSize];                    //create a temporary byte array of block size
            SysLib.rawread(blockNumber, tempBlock);                         //populate memory with block data
            for (int i = 0; i < Disk.blockSize; i++) { tempBlock[i] = 0; }  //iteratively clear out block data
            SysLib.int2bytes(freeList, tempBlock, 0);                       //set next block pointer
            SysLib.rawwrite(freeList, tempBlock);                           //sync returned block data to disk
            freeList = blockNumber;                                         //queue new block into freelist
            returned = true;
        }

        return returned;  //return success condition
    }

}   //end class
