/**
 * Created by jayse on 6/1/16.
 * Francis wrote the following methods in this class: Close, Delete, Format, FSize, and Sync
 * Jayse wrote the following methods in this class: Constructor, Open, Read, Seek, Write
 */
public class FileSystem
{
    private Directory directory;
    private SuperBlock superblock;
    private FileTable files;
    private final int SEEK_SET = 0;
    private final int SEEK_CUR = 1;
    private final int SEEK_END = 2;


    public FileSystem(int diskSize)
    {
        //(diskSize - 1) * 16;
        superblock = new SuperBlock(diskSize);
        directory = new Directory(superblock.maxInode());
        files = new FileTable(directory);
    }

    //Impact : Closes an existing file tht is open in the shared file table
    public boolean close(FileTableEntry fd)
    {
        //temp variable
        boolean goodFile = directory.isiNumberinUse(fd.iNumber);

        //check if fd is valid
        if (goodFile == true) {
            //check if fd is open
            if (files.isOpen(fd)) {
                //remove all open entries in the shared file table
                while (files.isOpen(fd))
                {
                    files.ffree(fd);  //remove an entry from the shared file table
                }
                SysLib.cout ( "File closed.");
                return true;
            } else {
                SysLib.cerr ("Close file error: File not open");
                return false;
            }
        }
        else
        {
            SysLib.cerr ("Close file error: File does not exist");
            return false;
        }
    }

    //Impact : deletes a file from the file system.  If the file exists, remove
    // from file table, release the iNode and all associated blocks, and wipe
    //  the entry from the directory.  then sync the file system to disk
    public boolean delete(String fileName)
    {
        //check to see if the file exists in the directory
        int fileID = (int)(directory.namei(fileName));

        //if the file exists...
        if (fileID != -1)
        {
            //...remove all entries in the file table
            files.deleteFTE(fileName);

            //...release blocks from the iNode
            //waiting on iNODE access functionality

            //...clear the directory
            directory.deleteDirFile(fileName);

            //...sync file system to disk
            sync();

            return true;
        }
        else
        {
            return false;
        }

    }

    //Impact : wipes the disk of all data and resets the file system
    public boolean format(int newFileCount)
    {
        boolean success = false;
        //call the superblock.format method
        success = superblock.format(newFileCount);

        //if the superblock was updated
        if (success == true)
        {
            //reset the filetable
            directory = new Directory(superblock.maxInode());
            //reset the file table
            files = new FileTable(directory);
        }
        return success;
    }

    //Impact : If the file exists in the directory returns the size of file
    // size from the inode, otherwise, returns '-1'
    public int fsize(FileTableEntry fd)
    {
        //temp variable
        boolean goodFile = directory.isiNumberinUse(fd.iNumber);

        //check if fd is valid
        if (goodFile == true)
        {
            return fd.inode.length;
        }
        else
        {
            return -1;
        }
    }

    public FileTableEntry open(String fileName, String mode)
    {
        //temp variable
        //check if file exists
        int iNumber = directory.namei(fileName);

        if(iNumber > 0)
        {
            FileTableEntry tempFile = files.falloc(fileName, mode);
            if(tempFile != null)
            {
                SysLib.cout("file opened");
            }
            else
                SysLib.cerr("open file error: file could not be opened");
            return tempFile;
        }
        else
        {
            iNumber = directory.ialloc(fileName);
            FileTableEntry tempFile = files.falloc(fileName, mode);
            if(tempFile != null)
            {
                SysLib.cout("file opened");
            }
            else
                SysLib.cerr("open file error: file could not be opened");
            return tempFile;
            //SysLib.cerr ("Open file error: File does not exist");
            //return null;
        }
    }

    public int read(FileTableEntry fd, byte buffer[])
    {
        int count = 0;
        byte[] tempBuffer = new byte[Disk.blockSize];
        int EndingBlock = fd.inode.findTargetBlock(fd.seekPtr + buffer.length);

        if(EndingBlock == -1)
            EndingBlock = fd.inode.findTargetBlock(fd.inode.length - 1);

        int StartingBlock = -1;

        do
        {
            StartingBlock = fd.inode.findTargetBlock(fd.seekPtr);
            int offset = fd.seekPtr - (Disk.blockSize * (fd.seekPtr / Disk.blockSize));
            SysLib.rawread(StartingBlock, tempBuffer);

            if(StartingBlock > 0)
            {
                for (int i = 0; i + offset < Disk.blockSize && count < buffer.length; i++, count++, fd.seekPtr++)
                {
                    buffer[count] = tempBuffer[i + offset];
                }
            }
            else
                break;

        } while (StartingBlock != EndingBlock);

        return count;
    }

    /*
        -1 = failure
        0 = success
     */
    public int seek(FileTableEntry fd, int offset, int whence)
    {
        switch (whence)
        {
            case SEEK_SET:
            {
                if (offset >= 0 && offset < fsize(fd))
                {
                    fd.seekPtr = offset;
                    break;
                }
                else if (offset < 0)
                {
                    offset = 0; //if user attempts to set the seek pointer to a
                    // negative number you must clamp it to zero
                    fd.seekPtr = offset;
                    break;
                }
                else
                {
                    offset = fsize(fd) - 1;
                    fd.seekPtr = offset;
                    break;
                    //if the user attempts to set the pointer to beyond a file size,
                    // you must set the seek pointer to the end of the file
                }
            }
            case SEEK_CUR:
            {
                if (fd.seekPtr + offset >= 0 && fd.seekPtr + offset < fsize(fd))
                {
                    fd.seekPtr += offset;
                    break;
                }
                else if (fd.seekPtr + offset < 0)
                {
                    fd.seekPtr = 0;
                    break;
                }
                else
                {
                    fd.seekPtr = fsize(fd) - 1;
                    break;
                }
            }
            case SEEK_END:
            {
                int end = fsize(fd) - 1;

                if (end + offset >= 0 && end + offset <= end)
                {
                    fd.seekPtr = end + offset;
                    break;
                }
                else if (end + offset < 0)
                {
                    fd.seekPtr = 0;
                    break;
                }
                else
                {
                    fd.seekPtr = end;
                    break;
                }
            }
            default:
            {
                return -1;
            }
        }
        return 0;
    }

    //Impact : Commits the file system metadata to disk
    public void sync()
    {
        //Step 1: commit open files to disk
        files.sync();

        //Step 2: commit the directory to disk
        directory.sync();

        //Step 3: commit the SuperBlock to disk
        superblock.sync();
    }

    public int write(FileTableEntry fd, byte buffer[])
    {
        if(fd.mode.equals("w"))
        {
            for (int i = 0; i < fd.inode.length; i += 512)
            {
                int blockNumber = fd.inode.findTargetBlock(i);

                if (blockNumber > 0)
                    superblock.returnBlock(blockNumber);
                else
                    break;
            }
            superblock.returnBlock(fd.inode.indirect);

            for (int i = 0; i <= 11; i++)
            {
                fd.inode.direct[i] = -1;
            }
            fd.inode.indirect = -1;

            byte[] tempBuffer = new byte[Disk.blockSize];

            for(int i = 0; i < buffer.length; i++)
            {
                if(i % Disk.blockSize == 0 && i != 0)
                {
                    short freeBlock = (short)superblock.getFreeBlock();
                    if(fd.inode.registerNewBlock(freeBlock) == 2)
                    {
                        freeBlock = (short)superblock.getFreeBlock();
                        fd.inode.registerNewBlock((freeBlock));
                    }

                    SysLib.rawwrite(freeBlock, tempBuffer);
                    tempBuffer = new byte[Disk.blockSize];
                    //write to disk
                }

                tempBuffer[i % 512] = buffer[i];
                fd.inode.length++;
            }
        }
        else if(fd.mode.equals("w+"))
        {
            byte[] tempBuffer = new byte[Disk.blockSize];
            SysLib.rawread(fd.inode.findTargetBlock(fd.seekPtr), tempBuffer);
            short targetBlock;

            for(int i = fd.seekPtr % Disk.blockSize; i < buffer.length; i++)
            {
                if(i % Disk.blockSize == 0 && i != 0)
                {
                    targetBlock = fd.inode.findTargetBlock(fd.seekPtr - 1);
                    if (targetBlock == -1)
                    {
                        targetBlock = (short) superblock.getFreeBlock();
                        if (fd.inode.registerNewBlock(targetBlock) == 2)
                        {
                            targetBlock = (short) superblock.getFreeBlock();
                            fd.inode.registerNewBlock((targetBlock));
                        }
                    }
                    SysLib.rawwrite(targetBlock, tempBuffer);
                }

                tempBuffer[i % 512] = buffer[i];
                fd.seekPtr++;
                fd.inode.length++;
            }
        }
        else if(fd.mode.equals("a"))
        {
            byte[] tempBuffer = new byte[Disk.blockSize];

            for (int i = 0; i < buffer.length; i++)
            {
                if (i % Disk.blockSize == 0 && i != 0)
                {
                    short freeBlock = (short) superblock.getFreeBlock();
                    if (fd.inode.registerNewBlock(freeBlock) == 2)
                    {
                        freeBlock = (short) superblock.getFreeBlock();
                        fd.inode.registerNewBlock((freeBlock));
                    }

                    SysLib.rawwrite(freeBlock, tempBuffer);
                    tempBuffer = new byte[Disk.blockSize];
                    //write to disk
                }

                tempBuffer[i % 512] = buffer[i];
                fd.inode.length++;
            }
        }
        else
        {
            return -1;
        }
        return 1;    /// dummy entry
    }

}
