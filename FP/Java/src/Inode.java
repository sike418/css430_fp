/**
 * Created by jayse on 6/1/16.
 * Jayse wrote all methods in this class.
 */
public class Inode
{
    private final static int iNodeSize = 32;       // fix to 32 bytes
    private final static int directSize = 11;      // # direct pointers

    //flag
    public int length;                             // file size in bytes
    public short count;                            // # file-table entries pointing to this
    public short flag;                             // 0 = unused, 1 = used, ...
    public short direct[] = new short[directSize]; // direct pointers
    public short indirect;                         // a indirect pointer

    Inode( )
    {                                     // a default constructor
        length = 0;
        count = 0;
        flag = 1;
        for ( int i = 0; i < directSize; i++ )
            direct[i] = -1;
        indirect = -1;
    }

    Inode( short iNumber, int dirBlocks)
    {
        int blockNumber = dirBlocks + iNumber / 16;
        byte[] data = new byte[Disk.blockSize];
        SysLib.rawread(blockNumber, data);
        int offset = (iNumber % 16) * 32;

        length = SysLib.bytes2int(data, offset);
        offset += 4;
        count = SysLib.bytes2short(data, offset);
        offset += 2;
        flag = SysLib.bytes2short(data, offset);
        offset += 2;

        for(int i = 0; i < directSize; i++, offset += 2)
        {
                direct[i] = SysLib.bytes2short(data, offset);   //retrieves each pointer
        }

        indirect = SysLib.bytes2short(data, offset);
    }

    void toDisk( short iNumber, int dirBlocks)
    {
        int blockNumber = dirBlocks + iNumber / 16;
        byte[] data = new byte[Disk.blockSize];
        SysLib.rawread(blockNumber, data);
        int offset = (iNumber % 16) * 32;

        SysLib.int2bytes(length, data, offset);
        offset += 4;
        SysLib.short2bytes(count, data, offset);
        offset += 2;
        SysLib.short2bytes(flag, data, offset);
        offset += 2;

        for(int i = 0; i < directSize; i++, offset += 2)
        {
            SysLib.short2bytes(direct[i], data, offset);   //retrieves each pointer
        }

        SysLib.short2bytes(indirect, data, offset);
    }

    short getIndexBlockNumber()
    {
        return indirect;
    }

    boolean setIndexBlockNumber(short indexBlockNumber)
    {
        if(indexBlockNumber > 0)
            indirect = indexBlockNumber;
        else
        {
            indirect = -1;
            return false;
        }
        return true;
    }

    //assuming this will only be called when a new block is needed
    short registerNewBlock(short blockNumber)
    {
        int numOfBlocks = length / Disk.blockSize;

        if (numOfBlocks >= 11 && indirect >= 0)//add to the index block
        {
            byte[] buffer = new byte[Disk.blockSize];
            SysLib.rawread(indirect, buffer);
            numOfBlocks -= 10;  //offset;
            int offset = numOfBlocks * 2;   //two bytes per pointer

            SysLib.short2bytes(blockNumber, buffer, offset);
            SysLib.rawwrite(indirect, buffer);

            return 0;
        }
        else if (numOfBlocks < 11)
            direct[numOfBlocks] = blockNumber;
        else    //no indirect block set.
        {
            return 2;
        }
        return 1;
    }

    short findTargetBlock(int offset)
    {
        int blk = offset / 512;

        if (blk >= 11 && indirect >= 0)//scan the index block
        {
            byte[] block = new byte[Disk.blockSize];
            SysLib.rawread(indirect, block);
            int indirectOffset = (blk - 11) * 2;    //two bytes per indirect pointer
            return SysLib.bytes2short(block, indirectOffset);
        }
        else if (blk < 11)
            return direct[blk];
        else    //no indirect block set.
            return -1;
    }

}
