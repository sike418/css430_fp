/**
 * Created by jayse on 6/1/16.
 * Francis wrote the following methods in this class: SYNC, DELETEDIRFILE
 * Jayse wrote the following methods in this class: Constructor, bytes2Directory,
 *  directory2bytes, IALLOC, IFREE, NAMEI, GETDIRBLOCKS, ISINUMBERINUSE
 */
import java.lang.Math;
import java.sql.Array;
import java.util.Arrays;

public class Directory
{
    //directory entries
    private static int maxChars = 30; // max characters of each file name
    private int fsize[];            // each element stores a different file name size.
    private char fnames[][];        // each element stores a different file name.

    public Directory( int maxInumber ) { // directory constructor
        fsize = new int[maxInumber];     // maxInumber = max files
        for ( int i = 0; i < maxInumber; i++ )
            fsize[i] = 0;                 // all file size initialized to 0
        fnames = new char[maxInumber][maxChars];
        String root = "/";                // entry(inode) 0 is "/"
        fsize[0] = root.length( );        // fsize[0] is the size of "/".
        root.getChars( 0, fsize[0], fnames[0], 0 ); // fnames[0] includes "/"
    }

    public void bytes2directory( byte data[] )
    {
        int offset = 0;
        for(int i = 0; i < fsize.length; i++, offset += 4)
            fsize[i] = SysLib.bytes2int(data, offset);
        for(int i = 0; i < fnames.length; i++, offset += maxChars * 2)
        {
            String fname = new String(data, offset, maxChars * 2);
            fname.getChars(0, fsize[i], fnames[i], 0);
        }
        // assumes data[] received directory information from disk
        // initializes the Directory instance with this data[]
    }

    public byte[] directory2bytes()
    {
        int numOfBlocks = getDirBlocks();
        byte[] tempDir = new byte[Disk.blockSize * numOfBlocks];
        int offset = 0;

        for (int index = 0; index < fsize.length; index++, offset += 4)
        {
            SysLib.int2bytes(fsize[index], tempDir, offset);
            byte[] tempByteName = new String(fnames[index]).getBytes();

            for (int count = 0; count < tempByteName.length; count++)
            {
                tempDir[((4*fsize.length) + index * 60) + count] = tempByteName[count];
            }
        }
        return tempDir;
    }

    public short ialloc( String filename )
    {
        for(short index = 0; index < fsize.length; index++)
            if(fsize[index] == 0)
            {
                if(filename.length() > maxChars)
                    fsize[index] = maxChars;
                else
                    fsize[index] = filename.length();
                filename.getChars(0, fsize[index], fnames[index], 0);
                return index;
            }
        return -1;
        // filename is the name of a file to be created.
        // allocates a new inode number for this filename
    }

    public boolean ifree( short iNumber )
    {
        if(iNumber < 0 || iNumber >= fsize.length)//invalid i number
            return false;
        else
        {
            fnames[iNumber] = new char[maxChars];
            fsize[iNumber] = 0;
        }
        return true;
    }

    public short namei( String filename )
    {

        for(short index = 0; index < fsize.length; index++)
        {
            if(fsize[index] == 0)
                continue;
            else
            {
                if(filename.equals(new String(fnames[index]).trim()))
                    return index;
            }
        }
        return -1;
        // returns the inumber corresponding to this filename
    }

    public int getDirBlocks()
    {
        return ((int) Math.ceil((double) fsize.length / (double) 8));
    }

    public boolean isiNumberinUse(int iNumber)
    {
        if(iNumber < 0 || iNumber >= fsize.length)
            return false;
        else if(fsize[iNumber] != 0)
            return true;
        else
            return false;
    }

    //Impact : Will commit the directory to disk
    public synchronized void sync()
    {
        //convert directory to a byte array
        byte[] completeDir = directory2bytes();
        //create a temporary block for commit operations
        byte[] tempBlock = new byte[Disk.blockSize];

        //iterate through the reserved blocks and commit the byte array to disk
        //determine the number of blocks reserved for directory
        int numBlocks = (completeDir.length%Disk.blockSize);

        //for each block
        for (int i = 1; i <= numBlocks; i++)
        {
            //for each byte
            int offset = ((i - 1) * Disk.blockSize);
            for (int j = 0; j < Disk.blockSize; j++)
            {
                //copy byte to tempBlock
                tempBlock[j] = completeDir[j + offset];
            }
            //commit directory segment to disk
            SysLib.rawwrite(i, tempBlock);
        }
    }

    //Impact : deletes a filename from the directory if it exists and returns
    // true.  otherwise, returns false.
    public synchronized boolean deleteDirFile (String fileName)
    {
        //check directory for the existence of the file and get the ID
        short found = namei(fileName);

        //if the file is in the directory...
        if (found != -1) {
            //...remove the entry from the directory
            ifree(found);
            return true;
        }
        else
        {
            return false;
        }
    }
}
